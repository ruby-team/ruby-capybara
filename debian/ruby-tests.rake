if RUBY_VERSION < "3.3"
  puts "I: skipping tests on ruby3.1"
  exit
end

require 'gem2deb/rake/spectask'

ENV.delete('CI')
ENV.delete('WEBDRIVERS')

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end
